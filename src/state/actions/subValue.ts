type SubAction = {
    type: 'SUB_VALUE';
  };
  
  export const subValue = (): SubAction => ({
    type: 'SUB_VALUE',
  });
