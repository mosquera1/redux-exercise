type TimesAction = {
    type: 'TIMES_VALUE';
  };
  
  export const timesValue = (): TimesAction => ({
    type: 'TIMES_VALUE',
  });
