type SqrAction = {
    type: 'SQR_VALUE';
  };
  
  export const sqrValue = (): SqrAction => ({
    type: 'SQR_VALUE',
  });
