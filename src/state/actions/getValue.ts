type GetValueAction = {
    type: 'GET_VALUE';
    payload: number;
  };
  
  export const getValue = (n: number): GetValueAction => ({
    type: 'GET_VALUE',
    payload: n
  });
