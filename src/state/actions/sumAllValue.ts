type SumAllAction = {
    type: 'SUMALL_VALUE';
};
  
export const sumAllValue = (): SumAllAction => ({
    type: 'SUMALL_VALUE',
});