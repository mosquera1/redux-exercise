import { combineReducers } from 'redux';

import { sampleReducer } from './sampleReducer';
import { calculadoraReducer } from './calculadoraReducer';
import { higherOrderReducer } from './higherOrderReducer';

export const rootReducer = higherOrderReducer(
  combineReducers({
    sample: sampleReducer,
    calcReducer: calcReducer,
  })
);

