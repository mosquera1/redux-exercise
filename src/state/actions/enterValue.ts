type EnterAction = {
    type: 'ENTER_VALUE';
  };
  
  export const enterValue = (): EnterAction => ({
    type: 'ENTER_VALUE',
  });
