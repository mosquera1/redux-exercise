import { Reducer } from 'redux';
import { AppAction } from '../AppAction';

type SampleState = {
  dot: boolean;
  history: number[][];
  stack: number[];
  valueCurrent: number;
};

const initialState: SampleState = {
  dot: false,
  history: [],
  stack: [],
  valueCurrent: 0,
};

export const sampleReducer: Reducer<SampleState, AppAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case 'CHANGE_SAMPLE':
      return { message: 'Sample' };
    default:
      return state;
  }
};