type SumAction = {
    type: 'SUM_VALUE';
  };
  
  export const sumValue = (): SumAction => ({
    type: 'SUM_VALUE',
  });
