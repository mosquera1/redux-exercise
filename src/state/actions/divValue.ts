type DivAction = {
    type: 'DIV_VALUE';
  };
  
  export const divValue = (): DivAction => ({
    type: 'DIV_VALUE',
  });